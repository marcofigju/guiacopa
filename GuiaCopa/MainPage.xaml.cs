﻿using GuiaCopa.Data;
using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace GuiaCopa
{
    public partial class MainPage : PhoneApplicationPage
    {
        private string caminhoImagemSelecionada;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            DataContext = App.bancoDados;
        }

        private void EstadioSelecionado(object sender, SelectionChangedEventArgs e)
        {
            if (MainLongListSelector == null)
            {
                return;
            }

            NavigationService.Navigate(new Uri("/EstadioInfo.xaml?estadioSelecionado=" + ((MainLongListSelector.SelectedItem as Estadio).Id - 1), UriKind.Relative));

            //MainLongListSelector.SelectedItem = null;
        }

        private void AdicionarEstadio(object sender, RoutedEventArgs e)
        {
            Estadio novoEstadio = new Estadio()
            {
                Nome = txtNome.Text,
                Cidade = txtCidade.Text,
                Descricao = txtDesc.Text,
                CaminhoImagem = caminhoImagemSelecionada
            };
            App.bancoDados.AdicionarEstadio(novoEstadio);

            txtNome.Text = "";
            txtCidade.Text = "";
            txtDesc.Text = "";
            pivot.SelectedIndex = 0;

        }

        private void Icone1Selected(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            caminhoImagemSelecionada = "/Imgs/arenabaixada.jpg";
        }
        private void Icone2Selected(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            caminhoImagemSelecionada = "/Imgs/arenacurintia.jpg";
        }
    }
}