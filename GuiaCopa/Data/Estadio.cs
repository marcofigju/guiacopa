﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiaCopa.Data
{
    [Table]
    public class Estadio : INotifyPropertyChanged
    {
        #region Variavies
       
        private int id;
        private string nome;
        private string cidade;
        private string descricao;
        private string caminhoImagem;
        private Uri imagem;

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Getters & Setters
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                if(value != id)
                {
                    id = value;
                    NotifyPropertyChanged("ID");
                }
            }
        }
        [Column]
        public string Nome
        {
            get
            {
                return nome;
            }
            set
            {
                if (value != nome)
                {
                    nome = value;
                    NotifyPropertyChanged("NOME");
                }
            }
        }
        [Column]
        public string Cidade
        {
            get
            {
                return cidade;
            }
            set
            {
                if (value != cidade)
                {
                    cidade = value;
                    NotifyPropertyChanged("CIDADE");
                }
            }
        }
        
        public Uri Imagem
        {
            get
            {
                return imagem;
            }
            set
            {
                if (value != imagem)
                {
                    imagem = value;
                    ;
                }
            }
        }

        [Column]
        public string Descricao
        {
            get
            {
                return descricao;
            }
            set
            {
                if (value != descricao)
                {
                    descricao = value;
                    NotifyPropertyChanged("DESCRICAO");
                }
            }
        }

        [Column]
        public string CaminhoImagem
        {
            get
            {
                return caminhoImagem;
            }

            set
            {
                caminhoImagem = value;
                Imagem = new Uri(caminhoImagem, UriKind.Relative);
            }
        }

        private void NotifyPropertyChanged(String PropertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion
    }

}
