﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiaCopa.Data
{
    public class ConnectionToDatabase : DataContext
    {
        public Table<Estadio> dados;
        public static string DBConnectionString = "Data Source=isostore:/Database.sdf";
        public ConnectionToDatabase(string ConnectionString) : base(ConnectionString)
        { }

    }

    public class MainDados : INotifyPropertyChanged
    {
        private ConnectionToDatabase connection;

        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<Estadio> ListaEstadios;
        public ObservableCollection<Estadio> listaEstadios
        {
            get
            {
                return ListaEstadios;
            }
            set
            {
                if (ListaEstadios != value)
                {
                    ListaEstadios = value;
                    NotifyPropertyChanged("listaEstadios");
                }
            }
        }

        private void NotifyPropertyChanged(String PropertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(PropertyName));
            }
        }

        public MainDados()
        {
            connection = new ConnectionToDatabase(ConnectionToDatabase.DBConnectionString);

            if (connection.DatabaseExists() == false)
            {
                connection.CreateDatabase();
            }
            QueryDados();
        }


        private void QueryDados()
        {
            var resultado = from Estadio item in connection.dados
                            orderby item.Id ascending
                            select item;
            listaEstadios = new ObservableCollection<Estadio>(resultado);
        }

        public void AdicionarEstadio(Estadio novoEstadio)
        {
            connection.dados.InsertOnSubmit(novoEstadio);
            connection.SubmitChanges();
            QueryDados();
        }
    }
}
